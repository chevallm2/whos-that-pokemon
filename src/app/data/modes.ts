export const MODES = [
    {
        label: 'Classique',
        value: 'classique',
        description: `
      <p><strong>Objectif:</strong> Obtenir le plus de points possible.</p>
      <p>Pour gagner des points:</p>
      <ul>
        <li>Deviner un pokémon correctement (+5)</li>
        <li>Faire une chaine de bonnes réponses (à chaque bonne réponse consécutive, vous gagnez 2% de points supplémentaires)</li>
      </ul>
      <p>Vous perdrez des points si:</p>
      <ul>
        <li>Vous utilisez un indice (-3)</li>
        <li>Vous vous trompez de réponse (-4)</li>
        <li>Vous passez (-10)</li>
      </ul>
      `
    },
    {
        label: 'Survie',
        value: 'survie',
        description: `
        <p><strong>Objectif:</strong> Marquer le plus de points avec un nombre de vies limités</p>
        <p>3 vies au départ.</p>
        <p>Vous perdez une vie quand:</p>
        <ul>
            <li>Vous vous trompez de réponse</li>
            <li>Vous utilisez 3 fois la fonction "indice"</li>
        </ul>
        <p>Vous récupérez une vie quand:</p>
        <ul>
            <li>Vous répondez 5 fois consécutivement</li>
            <li>Vous devinez 20 réponses</li>
        </ul>
        `
    },
    {
        label: 'Survie (hardcore)',
        value: 'survie-hc',
        description: `
        <p><strong>Objectif:</strong> Marquer le plus de points avec un nombre de vies limités</p>
        <p>1 vie au départ.</p>
        <p>Vous perdez la partie si vous vous trompez.</p>
        <p>Vous ne récupérez aucune vie.</p>
        <p>Vous ne pouvez pas utiliser la fonctionnalité indice.</p>
        `
    },
    {
        label: 'Time Attack',
        value: 'time-attack',
        description: `
        <p><strong>Objectif:</strong> Faire le plus de points avant que le chronomètre soit à 0.</p>
        <p>Vous gagnez 20 secondes si vous trouvez la bonne réponse.</p>
        <p>Utiliser un indice enlève 5 secondes au chronomètre.</p>
        `
    }
];
