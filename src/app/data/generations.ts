export const GENERATIONS = [
    {
      label: 'Génération 1',
      value: '1',
      min: 1,
      max: 151
    },
    {
      label: 'Génération 2',
      value: '2',
      min: 152,
      max: 251
    },
    {
      label: 'Génération 3',
      value: '3',
      min: 252,
      max: 386
    },
    {
      label: 'Génération 4',
      value: '4',
      min: 387,
      max: 493
    },
    {
      label: 'Génération 5',
      value: '5',
      min: 494,
      max: 649
    },
    {
      label: 'Génération 6',
      value: '6',
      min: 650,
      max: 721,
    },
    {
      label: 'Génération 7',
      value: '7',
      min: 722,
      max: 809,
    },
    {
      label: 'Génération 8',
      value: '8',
      min: 810,
      max: 898,
    }
];
