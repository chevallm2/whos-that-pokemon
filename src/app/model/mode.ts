export class Mode {
    label: string;
    value: string;
    description: string;

    constructor(init?: Partial<Mode>) {
        Object.assign(this, init);
    }
}
