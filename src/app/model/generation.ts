export class Generation {

    label: string;
    value: string;
    min: number;
    max: number;

    constructor(init?: Partial<Generation>) {
        Object.assign(this, init);
    }
}
