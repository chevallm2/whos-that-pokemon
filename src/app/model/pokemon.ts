export class Pokemon {
  id: number;
  en: string;
  fr: string;

  constructor(init?: Partial<Pokemon>) {
    Object.assign(this, init);
  }
}
