import { AfterViewInit, Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Pokemon } from '../model/pokemon';
import { names } from '../noms';
import { GuessEvent } from './guess-event';

@Component({
  selector: 'app-guesser',
  templateUrl: './guesser.component.html',
  styleUrls: ['./guesser.component.sass']
})
export class GuesserComponent implements OnInit, AfterViewInit {

  @Input()
  pokemonDispo: Pokemon[];

  @Input()
  indiceDisponible = Number.POSITIVE_INFINITY;

  @Output()
  guessed = new EventEmitter<GuessEvent>();

  pokemonId = null;
  pokemonName = null;

  private readonly TOKEN_REMPLIFY = '{pokemonId}';
  private readonly POKEMON_BASE_URL = `./assets/pokemon/${this.TOKEN_REMPLIFY}.png`;

  @ViewChild('search') searchBar: ElementRef<HTMLInputElement>;
  shouldShowPokemonName = false;
  shouldShowPokemonImage = false;
  @ViewChild('pokemonShadow') pokemonShadowCanvas: ElementRef<HTMLCanvasElement>;
  private pokemonShadowContext: CanvasRenderingContext2D;

  constructor() { }

  ngOnInit(): void {
    this.pokemonId = this.getRandomPokemonId();
  }

  ngAfterViewInit() {
    this.pokemonShadowContext = this.pokemonShadowCanvas.nativeElement.getContext('2d');
    this.play();
  }

  getPokemonSrc() {
    return this.POKEMON_BASE_URL.replace(this.TOKEN_REMPLIFY, this.getPokemonIdAsString());
  }

  newPokemon() {
    this.pokemonId = this.getRandomPokemonId();
    this.play();
  }

  getRandomPokemonId() {
    const min = Math.ceil(1);
    const max = Math.floor(this.pokemonDispo.length);
    console.log(min, max)
    return Math.floor(Math.random() * (max - min +1)) + min;
  }

  autocomplete(query: string) {
    if (query.length > 2) {
      this.pokemonDispo = names.filter(noms => {
        const normalizedQuery = this.normalize(query);
        return [noms.fr].some(nom => this.normalize(nom).includes(normalizedQuery));
      });
    } else {
      this.pokemonDispo = [];
    }

  }

  guess(name: string) {
    const pokemon = names.find(pokemon => pokemon.id === this.pokemonId);
    if (pokemon.fr.toLowerCase() === name.toLowerCase()) {
      this.clearCanvas();
      this.showImage();
      this.pokemonName = pokemon.fr;
      this.showPokemonName();
      this.guessed.emit({result: 'correct', pokemon});
      setTimeout(() => {
        this.hideImage();
        this.hidePokemonName();
        this.emptySearchBar();
        this.newPokemon();
      }, 2000);
    } else {
      this.guessed.emit({result: 'incorrect', pokemon});
      this.searchBar.nativeElement.classList.add('error');
      setTimeout(() => {
        this.searchBar.nativeElement.classList.remove('error');
      }, 2000);

    }
  }

  indice() {
    const ind = clickEvent => {
      if ((clickEvent.target as HTMLElement).tagName === 'CANVAS') {
        const coordinates = this.getCoordinatesOfClick(clickEvent);
        this.reveal(coordinates, 10);
        document.removeEventListener('click', ind);
      }
    };
    document.addEventListener('click', ind);
    if (this.indiceDisponible !== null) {
      --this.indiceDisponible;
    }
  }

  private emptySearchBar() {
    this.searchBar.nativeElement.value = '';
  }

  private play() {
    this.clearCanvas();
    this.hideImage();
    this.hidePokemonName();
    const image = this.getImage();
    image.onload = () => {
      this.pokemonShadowContext.drawImage(image, 0, 0);
      const imageData = this.pokemonShadowContext.getImageData(0, 0, 215, 215);
      this.creerOmbre(imageData);
      this.pokemonShadowContext.putImageData(imageData, 0, 0);
    }
  }

  private clearCanvas() {
    this.pokemonShadowContext.clearRect(0, 0, 215, 215);
  }

  private getPokemonIdAsString(): string {
    return ('000' + this.pokemonId).substr(-3);
  }

  private creerOmbre(imageData: ImageData) {
    for (let cursor = 0; cursor < imageData.data.length; cursor += 4) {
      const transparent = imageData.data[cursor+3] === 0;
      if (!transparent) {
        imageData.data[cursor] = 0;
        imageData.data[cursor+1] = 0;
        imageData.data[cursor+2] = 0;
      }
    }
  }

  private showImage() {
    this.shouldShowPokemonImage = true;
  }

  hideImage() {
    this.shouldShowPokemonImage = false;
  }

  private showPokemonName() {
    this.shouldShowPokemonName = true;
  }

  private hidePokemonName() {
    this.shouldShowPokemonName = false;
  }

  private getImage() {
    const imageSrc = this.getPokemonSrc();
    const image = new Image();
    image.src = imageSrc;
    return image;
  }

  private getCoordinatesOfClick(clickEvent: MouseEvent) {
    const rect = (clickEvent.target as HTMLCanvasElement).getBoundingClientRect();
    return {
      x: clickEvent.clientX - rect.left,
      y: clickEvent.clientY - rect.top
    }
  }

  private reveal(coord, ray: number) {
    this.showImage();
    this.pokemonShadowContext.globalCompositeOperation = 'destination-out';
    this.pokemonShadowContext.beginPath()
    this.pokemonShadowContext.arc(coord.x, coord.y, ray, 0, 360);
    this.pokemonShadowContext.fill();
    this.pokemonShadowContext.closePath();
    this.pokemonShadowContext.globalCompositeOperation = 'source-over';
  }

  private normalize(string: string) {
    return string.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

}
