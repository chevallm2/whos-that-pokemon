import { Pokemon } from "../model/pokemon";

export interface GuessEvent {
  result: 'correct' | 'incorrect';
  pokemon: Pokemon;
}
