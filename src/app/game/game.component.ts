import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Mode} from '../model/mode';
import {Generation} from '../model/generation';
import {MODES} from '../data/modes';
import {GENERATIONS} from '../data/generations';
import { Pokemon } from '../model/pokemon';
import { names } from '../noms';
import { GuessEvent } from '../guesser/guess-event';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit {

  mode: Mode;
  generations: Generation[];
  pokemonsAvailables: Pokemon[];
  pokemonsGuessed: Pokemon[];

  constructor(
      private readonly route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.pokemonsGuessed = [];
    const modeValue = this.route.snapshot.paramMap.get('mode');
    const generationsValues = this.route.snapshot.paramMap.get('generations').split(',');
    this.mode = MODES.find(mode => mode.value === modeValue);
    this.generations = GENERATIONS.filter(generation => generationsValues.includes(generation.value));
    this.loadPokemons();
  }

  loadPokemons() {
    this.pokemonsAvailables = names.filter((pokemon: Pokemon) => {
      const ranges = this.generations.map(generation => ({min: generation.min, max: generation.max}));
      const pokemonIsInRanges = ranges.some(({min, max}) => pokemon.id >= min && pokemon.id <= max);
      return pokemonIsInRanges;
    });
  }

  onGuess(event: GuessEvent) {
    if (event.result === 'correct') {
      const indexOfPokemon = this.pokemonsAvailables.indexOf(event.pokemon);
      console.log(indexOfPokemon);
      const deletedPokemon = this.pokemonsAvailables.splice(indexOfPokemon, 1);
      console.log(this.pokemonsAvailables);
      this.pokemonsGuessed.push(deletedPokemon[0]);
    }
  }

}
