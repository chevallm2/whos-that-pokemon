import { AfterViewInit, Component, ElementRef, NgProbeToken, OnInit, ViewChild } from '@angular/core';

import { names } from '../noms';
import { GENERATIONS } from '../data/generations';
import {MODES} from '../data/modes';
import {Mode} from '../model/mode';
import {Generation} from '../model/generation';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.sass']
})
export class AccueilComponent implements OnInit {

  modes = MODES;
  selectedMode: Mode;
  generations = GENERATIONS;
  selectedGenerations: Generation[];
  rules = null;
  shouldShowRules = false;

  constructor(
      private readonly router: Router
  ) { }

  ngOnInit() {
    this.selectMode(this.modes[0]);
    this.selectGenerations([...this.generations]);
  }

  setRules(rules: string) {
    if (this.rules === rules) {
      this.rules = null;
    } else {
      this.rules = rules;
    }
  }

  selectMode(mode: Mode) {
    this.setRules(mode.description);
    this.selectedMode = mode;
  }

  selectGenerations(generations: Generation[]) {
    this.selectedGenerations = generations;
  }

  updateSelectedGenerations(generation: Generation) {
    const index = this.selectedGenerations.findIndex(g => g.value === generation.value);
    if (index > -1) {
      this.selectedGenerations.splice(index, 1);
    } else {
      this.selectedGenerations.push(generation);
    }
  }

  launch() {
    const generations = this.selectedGenerations.map(g => g.value).join(',');
    this.router.navigate([this.selectedMode.value, generations]);
  }

  toggleRules() {
    this.shouldShowRules = !this.shouldShowRules;
  }


}
