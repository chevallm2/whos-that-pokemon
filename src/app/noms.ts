export const names = [
  {
    "id": 1,
    "en": "Bulbasaur",
    "fr": "Bulbizarre"
  },
  {
    "id": 2,
    "en": "Ivysaur",
    "fr": "Herbizarre"
  },
  {
    "id": 3,
    "en": "Venusaur",
    "fr": "Florizarre"
  },
  {
    "id": 4,
    "en": "Charmander",
    "fr": "Salamèche"
  },
  {
    "id": 5,
    "en": "Charmeleon",
    "fr": "Reptincel"
  },
  {
    "id": 6,
    "en": "Charizard",
    "fr": "Dracaufeu"
  },
  {
    "id": 7,
    "en": "Squirtle",
    "fr": "Carapuce"
  },
  {
    "id": 8,
    "en": "Wartortle",
    "fr": "Carabaffe"
  },
  {
    "id": 9,
    "en": "Blastoise",
    "fr": "Tortank"
  },
  {
    "id": 10,
    "en": "Caterpie",
    "fr": "Chenipan"
  },
  {
    "id": 11,
    "en": "Metapod",
    "fr": "Chrysacier"
  },
  {
    "id": 12,
    "en": "Butterfree",
    "fr": "Papilusion"
  },
  {
    "id": 13,
    "en": "Weedle",
    "fr": "Aspicot"
  },
  {
    "id": 14,
    "en": "Kakuna",
    "fr": "Coconfort"
  },
  {
    "id": 15,
    "en": "Beedrill",
    "fr": "Dardargnan"
  },
  {
    "id": 16,
    "en": "Pidgey",
    "fr": "Roucool"
  },
  {
    "id": 17,
    "en": "Pidgeotto",
    "fr": "Roucoups"
  },
  {
    "id": 18,
    "en": "Pidgeot",
    "fr": "Roucarnage"
  },
  {
    "id": 19,
    "en": "Rattata",
    "fr": "Rattata"
  },
  {
    "id": 20,
    "en": "Raticate",
    "fr": "Rattatac"
  },
  {
    "id": 21,
    "en": "Spearow",
    "fr": "Piafabec"
  },
  {
    "id": 22,
    "en": "Fearow",
    "fr": "Rapasdepic"
  },
  {
    "id": 23,
    "en": "Ekans",
    "fr": "Abo"
  },
  {
    "id": 24,
    "en": "Arbok",
    "fr": "Arbok"
  },
  {
    "id": 25,
    "en": "Pikachu",
    "fr": "Pikachu"
  },
  {
    "id": 26,
    "en": "Raichu",
    "fr": "Raichu"
  },
  {
    "id": 27,
    "en": "Sandshrew",
    "fr": "Sabelette"
  },
  {
    "id": 28,
    "en": "Sandslash",
    "fr": "Sablaireau"
  },
  {
    "id": 29,
    "en": "Nidoran♀",
    "fr": "Nidoran♀"
  },
  {
    "id": 30,
    "en": "Nidorina",
    "fr": "Nidorina"
  },
  {
    "id": 31,
    "en": "Nidoqueen",
    "fr": "Nidoqueen"
  },
  {
    "id": 32,
    "en": "Nidoran♂",
    "fr": "Nidoran♂"
  },
  {
    "id": 33,
    "en": "Nidorino",
    "fr": "Nidorino"
  },
  {
    "id": 34,
    "en": "Nidoking",
    "fr": "Nidoking"
  },
  {
    "id": 35,
    "en": "Clefairy",
    "fr": "Mélofée"
  },
  {
    "id": 36,
    "en": "Clefable",
    "fr": "Mélodelfe"
  },
  {
    "id": 37,
    "en": "Vulpix",
    "fr": "Goupix"
  },
  {
    "id": 38,
    "en": "Ninetales",
    "fr": "Feunard"
  },
  {
    "id": 39,
    "en": "Jigglypuff",
    "fr": "Rondoudou"
  },
  {
    "id": 40,
    "en": "Wigglytuff",
    "fr": "Grodoudou"
  },
  {
    "id": 41,
    "en": "Zubat",
    "fr": "Nosferapti"
  },
  {
    "id": 42,
    "en": "Golbat",
    "fr": "Nosferalto"
  },
  {
    "id": 43,
    "en": "Oddish",
    "fr": "Mystherbe"
  },
  {
    "id": 44,
    "en": "Gloom",
    "fr": "Ortide"
  },
  {
    "id": 45,
    "en": "Vileplume",
    "fr": "Rafflesia"
  },
  {
    "id": 46,
    "en": "Paras",
    "fr": "Paras"
  },
  {
    "id": 47,
    "en": "Parasect",
    "fr": "Parasect"
  },
  {
    "id": 48,
    "en": "Venonat",
    "fr": "Mimitoss"
  },
  {
    "id": 49,
    "en": "Venomoth",
    "fr": "Aéromite"
  },
  {
    "id": 50,
    "en": "Diglett",
    "fr": "Taupiqueur"
  },
  {
    "id": 51,
    "en": "Dugtrio",
    "fr": "Triopikeur"
  },
  {
    "id": 52,
    "en": "Meowth",
    "fr": "Miaouss"
  },
  {
    "id": 53,
    "en": "Persian",
    "fr": "Persian"
  },
  {
    "id": 54,
    "en": "Psyduck",
    "fr": "Psykokwak"
  },
  {
    "id": 55,
    "en": "Golduck",
    "fr": "Akwakwak"
  },
  {
    "id": 56,
    "en": "Mankey",
    "fr": "Férosinge"
  },
  {
    "id": 57,
    "en": "Primeape",
    "fr": "Colossinge"
  },
  {
    "id": 58,
    "en": "Growlithe",
    "fr": "Caninos"
  },
  {
    "id": 59,
    "en": "Arcanine",
    "fr": "Arcanin"
  },
  {
    "id": 60,
    "en": "Poliwag",
    "fr": "Ptitard"
  },
  {
    "id": 61,
    "en": "Poliwhirl",
    "fr": "Têtarte"
  },
  {
    "id": 62,
    "en": "Poliwrath",
    "fr": "Tartard"
  },
  {
    "id": 63,
    "en": "Abra",
    "fr": "Abra"
  },
  {
    "id": 64,
    "en": "Kadabra",
    "fr": "Kadabra"
  },
  {
    "id": 65,
    "en": "Alakazam",
    "fr": "Alakazam"
  },
  {
    "id": 66,
    "en": "Machop",
    "fr": "Machoc"
  },
  {
    "id": 67,
    "en": "Machoke",
    "fr": "Machopeur"
  },
  {
    "id": 68,
    "en": "Machamp",
    "fr": "Mackogneur"
  },
  {
    "id": 69,
    "en": "Bellsprout",
    "fr": "Chétiflor"
  },
  {
    "id": 70,
    "en": "Weepinbell",
    "fr": "Boustiflor"
  },
  {
    "id": 71,
    "en": "Victreebel",
    "fr": "Empiflor"
  },
  {
    "id": 72,
    "en": "Tentacool",
    "fr": "Tentacool"
  },
  {
    "id": 73,
    "en": "Tentacruel",
    "fr": "Tentacruel"
  },
  {
    "id": 74,
    "en": "Geodude",
    "fr": "Racaillou"
  },
  {
    "id": 75,
    "en": "Graveler",
    "fr": "Gravalanch"
  },
  {
    "id": 76,
    "en": "Golem",
    "fr": "Grolem"
  },
  {
    "id": 77,
    "en": "Ponyta",
    "fr": "Ponyta"
  },
  {
    "id": 78,
    "en": "Rapidash",
    "fr": "Galopa"
  },
  {
    "id": 79,
    "en": "Slowpoke",
    "fr": "Ramoloss"
  },
  {
    "id": 80,
    "en": "Slowbro",
    "fr": "Flagadoss"
  },
  {
    "id": 81,
    "en": "Magnemite",
    "fr": "Magnéti"
  },
  {
    "id": 82,
    "en": "Magneton",
    "fr": "Magnéton"
  },
  {
    "id": 83,
    "en": "Farfetch’d",
    "fr": "Canarticho"
  },
  {
    "id": 84,
    "en": "Doduo",
    "fr": "Doduo"
  },
  {
    "id": 85,
    "en": "Dodrio",
    "fr": "Dodrio"
  },
  {
    "id": 86,
    "en": "Seel",
    "fr": "Otaria"
  },
  {
    "id": 87,
    "en": "Dewgong",
    "fr": "Lamantine"
  },
  {
    "id": 88,
    "en": "Grimer",
    "fr": "Tadmorv"
  },
  {
    "id": 89,
    "en": "Muk",
    "fr": "Grotadmorv"
  },
  {
    "id": 90,
    "en": "Shellder",
    "fr": "Kokiyas"
  },
  {
    "id": 91,
    "en": "Cloyster",
    "fr": "Crustabri"
  },
  {
    "id": 92,
    "en": "Gastly",
    "fr": "Fantominus"
  },
  {
    "id": 93,
    "en": "Haunter",
    "fr": "Spectrum"
  },
  {
    "id": 94,
    "en": "Gengar",
    "fr": "Ectoplasma"
  },
  {
    "id": 95,
    "en": "Onix",
    "fr": "Onix"
  },
  {
    "id": 96,
    "en": "Drowzee",
    "fr": "Soporifik"
  },
  {
    "id": 97,
    "en": "Hypno",
    "fr": "Hypnomade"
  },
  {
    "id": 98,
    "en": "Krabby",
    "fr": "Krabby"
  },
  {
    "id": 99,
    "en": "Kingler",
    "fr": "Krabboss"
  },
  {
    "id": 100,
    "en": "Voltorb",
    "fr": "Voltorbe"
  },
  {
    "id": 101,
    "en": "Electrode",
    "fr": "Électrode"
  },
  {
    "id": 102,
    "en": "Exeggcute",
    "fr": "Noeunoeuf"
  },
  {
    "id": 103,
    "en": "Exeggutor",
    "fr": "Noadkoko"
  },
  {
    "id": 104,
    "en": "Cubone",
    "fr": "Osselait"
  },
  {
    "id": 105,
    "en": "Marowak",
    "fr": "Ossatueur"
  },
  {
    "id": 106,
    "en": "Hitmonlee",
    "fr": "Kicklee"
  },
  {
    "id": 107,
    "en": "Hitmonchan",
    "fr": "Tygnon"
  },
  {
    "id": 108,
    "en": "Lickitung",
    "fr": "Excelangue"
  },
  {
    "id": 109,
    "en": "Koffing",
    "fr": "Smogo"
  },
  {
    "id": 110,
    "en": "Weezing",
    "fr": "Smogogo"
  },
  {
    "id": 111,
    "en": "Rhyhorn",
    "fr": "Rhinocorne"
  },
  {
    "id": 112,
    "en": "Rhydon",
    "fr": "Rhinoféros"
  },
  {
    "id": 113,
    "en": "Chansey",
    "fr": "Leveinard"
  },
  {
    "id": 114,
    "en": "Tangela",
    "fr": "Saquedeneu"
  },
  {
    "id": 115,
    "en": "Kangaskhan",
    "fr": "Kangourex"
  },
  {
    "id": 116,
    "en": "Horsea",
    "fr": "Hypotrempe"
  },
  {
    "id": 117,
    "en": "Seadra",
    "fr": "Hypocéan"
  },
  {
    "id": 118,
    "en": "Goldeen",
    "fr": "Poissirène"
  },
  {
    "id": 119,
    "en": "Seaking",
    "fr": "Poissoroy"
  },
  {
    "id": 120,
    "en": "Staryu",
    "fr": "Stari"
  },
  {
    "id": 121,
    "en": "Starmie",
    "fr": "Staross"
  },
  {
    "id": 122,
    "en": "Mr. Mime",
    "fr": "M. Mime"
  },
  {
    "id": 123,
    "en": "Scyther",
    "fr": "Insécateur"
  },
  {
    "id": 124,
    "en": "Jynx",
    "fr": "Lippoutou"
  },
  {
    "id": 125,
    "en": "Electabuzz",
    "fr": "Élektek"
  },
  {
    "id": 126,
    "en": "Magmar",
    "fr": "Magmar"
  },
  {
    "id": 127,
    "en": "Pinsir",
    "fr": "Scarabrute"
  },
  {
    "id": 128,
    "en": "Tauros",
    "fr": "Tauros"
  },
  {
    "id": 129,
    "en": "Magikarp",
    "fr": "Magicarpe"
  },
  {
    "id": 130,
    "en": "Gyarados",
    "fr": "Léviator"
  },
  {
    "id": 131,
    "en": "Lapras",
    "fr": "Lokhlass"
  },
  {
    "id": 132,
    "en": "Ditto",
    "fr": "Métamorph"
  },
  {
    "id": 133,
    "en": "Eevee",
    "fr": "Évoli"
  },
  {
    "id": 134,
    "en": "Vaporeon",
    "fr": "Aquali"
  },
  {
    "id": 135,
    "en": "Jolteon",
    "fr": "Voltali"
  },
  {
    "id": 136,
    "en": "Flareon",
    "fr": "Pyroli"
  },
  {
    "id": 137,
    "en": "Porygon",
    "fr": "Porygon"
  },
  {
    "id": 138,
    "en": "Omanyte",
    "fr": "Amonita"
  },
  {
    "id": 139,
    "en": "Omastar",
    "fr": "Amonistar"
  },
  {
    "id": 140,
    "en": "Kabuto",
    "fr": "Kabuto"
  },
  {
    "id": 141,
    "en": "Kabutops",
    "fr": "Kabutops"
  },
  {
    "id": 142,
    "en": "Aerodactyl",
    "fr": "Ptéra"
  },
  {
    "id": 143,
    "en": "Snorlax",
    "fr": "Ronflex"
  },
  {
    "id": 144,
    "en": "Articuno",
    "fr": "Artikodin"
  },
  {
    "id": 145,
    "en": "Zapdos",
    "fr": "Électhor"
  },
  {
    "id": 146,
    "en": "Moltres",
    "fr": "Sulfura"
  },
  {
    "id": 147,
    "en": "Dratini",
    "fr": "Minidraco"
  },
  {
    "id": 148,
    "en": "Dragonair",
    "fr": "Draco"
  },
  {
    "id": 149,
    "en": "Dragonite",
    "fr": "Dracolosse"
  },
  {
    "id": 150,
    "en": "Mewtwo",
    "fr": "Mewtwo"
  },
  {
    "id": 151,
    "en": "Mew",
    "fr": "Mew"
  },
  {
    "id": 152,
    "en": "Chikorita",
    "fr": "Germignon"
  },
  {
    "id": 153,
    "en": "Bayleef",
    "fr": "Macronium"
  },
  {
    "id": 154,
    "en": "Meganium",
    "fr": "Méganium"
  },
  {
    "id": 155,
    "en": "Cyndaquil",
    "fr": "Héricendre"
  },
  {
    "id": 156,
    "en": "Quilava",
    "fr": "Feurisson"
  },
  {
    "id": 157,
    "en": "Typhlosion",
    "fr": "Typhlosion"
  },
  {
    "id": 158,
    "en": "Totodile",
    "fr": "Kaiminus"
  },
  {
    "id": 159,
    "en": "Croconaw",
    "fr": "Crocrodil"
  },
  {
    "id": 160,
    "en": "Feraligatr",
    "fr": "Aligatueur"
  },
  {
    "id": 161,
    "en": "Sentret",
    "fr": "Fouinette"
  },
  {
    "id": 162,
    "en": "Furret",
    "fr": "Fouinar"
  },
  {
    "id": 163,
    "en": "Hoothoot",
    "fr": "Hoothoot"
  },
  {
    "id": 164,
    "en": "Noctowl",
    "fr": "Noarfang"
  },
  {
    "id": 165,
    "en": "Ledyba",
    "fr": "Coxy"
  },
  {
    "id": 166,
    "en": "Ledian",
    "fr": "Coxyclaque"
  },
  {
    "id": 167,
    "en": "Spinarak",
    "fr": "Mimigal"
  },
  {
    "id": 168,
    "en": "Ariados",
    "fr": "Migalos"
  },
  {
    "id": 169,
    "en": "Crobat",
    "fr": "Nostenfer"
  },
  {
    "id": 170,
    "en": "Chinchou",
    "fr": "Loupio"
  },
  {
    "id": 171,
    "en": "Lanturn",
    "fr": "Lanturn"
  },
  {
    "id": 172,
    "en": "Pichu",
    "fr": "Pichu"
  },
  {
    "id": 173,
    "en": "Cleffa",
    "fr": "Mélo"
  },
  {
    "id": 174,
    "en": "Igglybuff",
    "fr": "Toudoudou"
  },
  {
    "id": 175,
    "en": "Togepi",
    "fr": "Togepi"
  },
  {
    "id": 176,
    "en": "Togetic",
    "fr": "Togetic"
  },
  {
    "id": 177,
    "en": "Natu",
    "fr": "Natu"
  },
  {
    "id": 178,
    "en": "Xatu",
    "fr": "Xatu"
  },
  {
    "id": 179,
    "en": "Mareep",
    "fr": "Wattouat"
  },
  {
    "id": 180,
    "en": "Flaaffy",
    "fr": "Lainergie"
  },
  {
    "id": 181,
    "en": "Ampharos",
    "fr": "Pharamp"
  },
  {
    "id": 182,
    "en": "Bellossom",
    "fr": "Joliflor"
  },
  {
    "id": 183,
    "en": "Marill",
    "fr": "Marill"
  },
  {
    "id": 184,
    "en": "Azumarill",
    "fr": "Azumarill"
  },
  {
    "id": 185,
    "en": "Sudowoodo",
    "fr": "Simularbre"
  },
  {
    "id": 186,
    "en": "Politoed",
    "fr": "Tarpaud"
  },
  {
    "id": 187,
    "en": "Hoppip",
    "fr": "Granivol"
  },
  {
    "id": 188,
    "en": "Skiploom",
    "fr": "Floravol"
  },
  {
    "id": 189,
    "en": "Jumpluff",
    "fr": "Cotovol"
  },
  {
    "id": 190,
    "en": "Aipom",
    "fr": "Capumain"
  },
  {
    "id": 191,
    "en": "Sunkern",
    "fr": "Tournegrin"
  },
  {
    "id": 192,
    "en": "Sunflora",
    "fr": "Héliatronc"
  },
  {
    "id": 193,
    "en": "Yanma",
    "fr": "Yanma"
  },
  {
    "id": 194,
    "en": "Wooper",
    "fr": "Axoloto"
  },
  {
    "id": 195,
    "en": "Quagsire",
    "fr": "Maraiste"
  },
  {
    "id": 196,
    "en": "Espeon",
    "fr": "Mentali"
  },
  {
    "id": 197,
    "en": "Umbreon",
    "fr": "Noctali"
  },
  {
    "id": 198,
    "en": "Murkrow",
    "fr": "Cornèbre"
  },
  {
    "id": 199,
    "en": "Slowking",
    "fr": "Roigada"
  },
  {
    "id": 200,
    "en": "Misdreavus",
    "fr": "Feuforêve"
  },
  {
    "id": 201,
    "en": "Unown",
    "fr": "Zarbi"
  },
  {
    "id": 202,
    "en": "Wobbuffet",
    "fr": "Qulbutoké"
  },
  {
    "id": 203,
    "en": "Girafarig",
    "fr": "Girafarig"
  },
  {
    "id": 204,
    "en": "Pineco",
    "fr": "Pomdepik"
  },
  {
    "id": 205,
    "en": "Forretress",
    "fr": "Foretress"
  },
  {
    "id": 206,
    "en": "Dunsparce",
    "fr": "Insolourdo"
  },
  {
    "id": 207,
    "en": "Gligar",
    "fr": "Scorplane"
  },
  {
    "id": 208,
    "en": "Steelix",
    "fr": "Steelix"
  },
  {
    "id": 209,
    "en": "Snubbull",
    "fr": "Snubbull"
  },
  {
    "id": 210,
    "en": "Granbull",
    "fr": "Granbull"
  },
  {
    "id": 211,
    "en": "Qwilfish",
    "fr": "Qwilfish"
  },
  {
    "id": 212,
    "en": "Scizor",
    "fr": "Cizayox"
  },
  {
    "id": 213,
    "en": "Shuckle",
    "fr": "Caratroc"
  },
  {
    "id": 214,
    "en": "Heracross",
    "fr": "Scarhino"
  },
  {
    "id": 215,
    "en": "Sneasel",
    "fr": "Farfuret"
  },
  {
    "id": 216,
    "en": "Teddiursa",
    "fr": "Teddiursa"
  },
  {
    "id": 217,
    "en": "Ursaring",
    "fr": "Ursaring"
  },
  {
    "id": 218,
    "en": "Slugma",
    "fr": "Limagma"
  },
  {
    "id": 219,
    "en": "Magcargo",
    "fr": "Volcaropod"
  },
  {
    "id": 220,
    "en": "Swinub",
    "fr": "Marcacrin"
  },
  {
    "id": 221,
    "en": "Piloswine",
    "fr": "Cochignon"
  },
  {
    "id": 222,
    "en": "Corsola",
    "fr": "Corayon"
  },
  {
    "id": 223,
    "en": "Remoraid",
    "fr": "Rémoraid"
  },
  {
    "id": 224,
    "en": "Octillery",
    "fr": "Octillery"
  },
  {
    "id": 225,
    "en": "Delibird",
    "fr": "Cadoizo"
  },
  {
    "id": 226,
    "en": "Mantine",
    "fr": "Démanta"
  },
  {
    "id": 227,
    "en": "Skarmory",
    "fr": "Airmure"
  },
  {
    "id": 228,
    "en": "Houndour",
    "fr": "Malosse"
  },
  {
    "id": 229,
    "en": "Houndoom",
    "fr": "Démolosse"
  },
  {
    "id": 230,
    "en": "Kingdra",
    "fr": "Hyporoi"
  },
  {
    "id": 231,
    "en": "Phanpy",
    "fr": "Phanpy"
  },
  {
    "id": 232,
    "en": "Donphan",
    "fr": "Donphan"
  },
  {
    "id": 233,
    "en": "Porygon2",
    "fr": "Porygon2"
  },
  {
    "id": 234,
    "en": "Stantler",
    "fr": "Cerfrousse"
  },
  {
    "id": 235,
    "en": "Smeargle",
    "fr": "Queulorior"
  },
  {
    "id": 236,
    "en": "Tyrogue",
    "fr": "Debugant"
  },
  {
    "id": 237,
    "en": "Hitmontop",
    "fr": "Kapoera"
  },
  {
    "id": 238,
    "en": "Smoochum",
    "fr": "Lippouti"
  },
  {
    "id": 239,
    "en": "Elekid",
    "fr": "Élekid"
  },
  {
    "id": 240,
    "en": "Magby",
    "fr": "Magby"
  },
  {
    "id": 241,
    "en": "Miltank",
    "fr": "Écrémeuh"
  },
  {
    "id": 242,
    "en": "Blissey",
    "fr": "Leuphorie"
  },
  {
    "id": 243,
    "en": "Raikou",
    "fr": "Raikou"
  },
  {
    "id": 244,
    "en": "Entei",
    "fr": "Entei"
  },
  {
    "id": 245,
    "en": "Suicune",
    "fr": "Suicune"
  },
  {
    "id": 246,
    "en": "Larvitar",
    "fr": "Embrylex"
  },
  {
    "id": 247,
    "en": "Pupitar",
    "fr": "Ymphect"
  },
  {
    "id": 248,
    "en": "Tyranitar",
    "fr": "Tyranocif"
  },
  {
    "id": 249,
    "en": "Lugia",
    "fr": "Lugia"
  },
  {
    "id": 250,
    "en": "Ho-Oh",
    "fr": "Ho-Oh"
  },
  {
    "id": 251,
    "en": "Celebi",
    "fr": "Celebi"
  },
  {
    "id": 252,
    "en": "Treecko",
    "fr": "Arcko"
  },
  {
    "id": 253,
    "en": "Grovyle",
    "fr": "Massko"
  },
  {
    "id": 254,
    "en": "Sceptile",
    "fr": "Jungko"
  },
  {
    "id": 255,
    "en": "Torchic",
    "fr": "Poussifeu"
  },
  {
    "id": 256,
    "en": "Combusken",
    "fr": "Galifeu"
  },
  {
    "id": 257,
    "en": "Blaziken",
    "fr": "Braségali"
  },
  {
    "id": 258,
    "en": "Mudkip",
    "fr": "Gobou"
  },
  {
    "id": 259,
    "en": "Marshtomp",
    "fr": "Flobio"
  },
  {
    "id": 260,
    "en": "Swampert",
    "fr": "Laggron"
  },
  {
    "id": 261,
    "en": "Poochyena",
    "fr": "Medhyèna"
  },
  {
    "id": 262,
    "en": "Mightyena",
    "fr": "Grahyèna"
  },
  {
    "id": 263,
    "en": "Zigzagoon",
    "fr": "Zigzaton"
  },
  {
    "id": 264,
    "en": "Linoone",
    "fr": "Linéon"
  },
  {
    "id": 265,
    "en": "Wurmple",
    "fr": "Chenipotte"
  },
  {
    "id": 266,
    "en": "Silcoon",
    "fr": "Armulys"
  },
  {
    "id": 267,
    "en": "Beautifly",
    "fr": "Charmillon"
  },
  {
    "id": 268,
    "en": "Cascoon",
    "fr": "Blindalys"
  },
  {
    "id": 269,
    "en": "Dustox",
    "fr": "Papinox"
  },
  {
    "id": 270,
    "en": "Lotad",
    "fr": "Nénupiot"
  },
  {
    "id": 271,
    "en": "Lombre",
    "fr": "Lombre"
  },
  {
    "id": 272,
    "en": "Ludicolo",
    "fr": "Ludicolo"
  },
  {
    "id": 273,
    "en": "Seedot",
    "fr": "Grainipiot"
  },
  {
    "id": 274,
    "en": "Nuzleaf",
    "fr": "Pifeuil"
  },
  {
    "id": 275,
    "en": "Shiftry",
    "fr": "Tengalice"
  },
  {
    "id": 276,
    "en": "Taillow",
    "fr": "Nirondelle"
  },
  {
    "id": 277,
    "en": "Swellow",
    "fr": "Hélédelle"
  },
  {
    "id": 278,
    "en": "Wingull",
    "fr": "Goélise"
  },
  {
    "id": 279,
    "en": "Pelipper",
    "fr": "Bekipan"
  },
  {
    "id": 280,
    "en": "Ralts",
    "fr": "Tarsal"
  },
  {
    "id": 281,
    "en": "Kirlia",
    "fr": "Kirlia"
  },
  {
    "id": 282,
    "en": "Gardevoir",
    "fr": "Gardevoir"
  },
  {
    "id": 283,
    "en": "Surskit",
    "fr": "Arakdo"
  },
  {
    "id": 284,
    "en": "Masquerain",
    "fr": "Maskadra"
  },
  {
    "id": 285,
    "en": "Shroomish",
    "fr": "Balignon"
  },
  {
    "id": 286,
    "en": "Breloom",
    "fr": "Chapignon"
  },
  {
    "id": 287,
    "en": "Slakoth",
    "fr": "Parecool"
  },
  {
    "id": 288,
    "en": "Vigoroth",
    "fr": "Vigoroth"
  },
  {
    "id": 289,
    "en": "Slaking",
    "fr": "Monaflèmit"
  },
  {
    "id": 290,
    "en": "Nincada",
    "fr": "Ningale"
  },
  {
    "id": 291,
    "en": "Ninjask",
    "fr": "Ninjask"
  },
  {
    "id": 292,
    "en": "Shedinja",
    "fr": "Munja"
  },
  {
    "id": 293,
    "en": "Whismur",
    "fr": "Chuchmur"
  },
  {
    "id": 294,
    "en": "Loudred",
    "fr": "Ramboum"
  },
  {
    "id": 295,
    "en": "Exploud",
    "fr": "Brouhabam"
  },
  {
    "id": 296,
    "en": "Makuhita",
    "fr": "Makuhita"
  },
  {
    "id": 297,
    "en": "Hariyama",
    "fr": "Hariyama"
  },
  {
    "id": 298,
    "en": "Azurill",
    "fr": "Azurill"
  },
  {
    "id": 299,
    "en": "Nosepass",
    "fr": "Tarinor"
  },
  {
    "id": 300,
    "en": "Skitty",
    "fr": "Skitty"
  },
  {
    "id": 301,
    "en": "Delcatty",
    "fr": "Delcatty"
  },
  {
    "id": 302,
    "en": "Sableye",
    "fr": "Ténéfix"
  },
  {
    "id": 303,
    "en": "Mawile",
    "fr": "Mysdibule"
  },
  {
    "id": 304,
    "en": "Aron",
    "fr": "Galekid"
  },
  {
    "id": 305,
    "en": "Lairon",
    "fr": "Galegon"
  },
  {
    "id": 306,
    "en": "Aggron",
    "fr": "Galeking"
  },
  {
    "id": 307,
    "en": "Meditite",
    "fr": "Méditikka"
  },
  {
    "id": 308,
    "en": "Medicham",
    "fr": "Charmina"
  },
  {
    "id": 309,
    "en": "Electrike",
    "fr": "Dynavolt"
  },
  {
    "id": 310,
    "en": "Manectric",
    "fr": "Élecsprint"
  },
  {
    "id": 311,
    "en": "Plusle",
    "fr": "Posipi"
  },
  {
    "id": 312,
    "en": "Minun",
    "fr": "Négapi"
  },
  {
    "id": 313,
    "en": "Volbeat",
    "fr": "Muciole"
  },
  {
    "id": 314,
    "en": "Illumise",
    "fr": "Lumivole"
  },
  {
    "id": 315,
    "en": "Roselia",
    "fr": "Rosélia"
  },
  {
    "id": 316,
    "en": "Gulpin",
    "fr": "Gloupti"
  },
  {
    "id": 317,
    "en": "Swalot",
    "fr": "Avaltout"
  },
  {
    "id": 318,
    "en": "Carvanha",
    "fr": "Carvanha"
  },
  {
    "id": 319,
    "en": "Sharpedo",
    "fr": "Sharpedo"
  },
  {
    "id": 320,
    "en": "Wailmer",
    "fr": "Wailmer"
  },
  {
    "id": 321,
    "en": "Wailord",
    "fr": "Wailord"
  },
  {
    "id": 322,
    "en": "Numel",
    "fr": "Chamallot"
  },
  {
    "id": 323,
    "en": "Camerupt",
    "fr": "Camérupt"
  },
  {
    "id": 324,
    "en": "Torkoal",
    "fr": "Chartor"
  },
  {
    "id": 325,
    "en": "Spoink",
    "fr": "Spoink"
  },
  {
    "id": 326,
    "en": "Grumpig",
    "fr": "Groret"
  },
  {
    "id": 327,
    "en": "Spinda",
    "fr": "Spinda"
  },
  {
    "id": 328,
    "en": "Trapinch",
    "fr": "Kraknoix"
  },
  {
    "id": 329,
    "en": "Vibrava",
    "fr": "Vibraninf"
  },
  {
    "id": 330,
    "en": "Flygon",
    "fr": "Libégon"
  },
  {
    "id": 331,
    "en": "Cacnea",
    "fr": "Cacnea"
  },
  {
    "id": 332,
    "en": "Cacturne",
    "fr": "Cacturne"
  },
  {
    "id": 333,
    "en": "Swablu",
    "fr": "Tylton"
  },
  {
    "id": 334,
    "en": "Altaria",
    "fr": "Altaria"
  },
  {
    "id": 335,
    "en": "Zangoose",
    "fr": "Mangriff"
  },
  {
    "id": 336,
    "en": "Seviper",
    "fr": "Séviper"
  },
  {
    "id": 337,
    "en": "Lunatone",
    "fr": "Séléroc"
  },
  {
    "id": 338,
    "en": "Solrock",
    "fr": "Solaroc"
  },
  {
    "id": 339,
    "en": "Barboach",
    "fr": "Barloche"
  },
  {
    "id": 340,
    "en": "Whiscash",
    "fr": "Barbicha"
  },
  {
    "id": 341,
    "en": "Corphish",
    "fr": "Écrapince"
  },
  {
    "id": 342,
    "en": "Crawdaunt",
    "fr": "Colhomard"
  },
  {
    "id": 343,
    "en": "Baltoy",
    "fr": "Balbuto"
  },
  {
    "id": 344,
    "en": "Claydol",
    "fr": "Kaorine"
  },
  {
    "id": 345,
    "en": "Lileep",
    "fr": "Lilia"
  },
  {
    "id": 346,
    "en": "Cradily",
    "fr": "Vacilys"
  },
  {
    "id": 347,
    "en": "Anorith",
    "fr": "Anorith"
  },
  {
    "id": 348,
    "en": "Armaldo",
    "fr": "Armaldo"
  },
  {
    "id": 349,
    "en": "Feebas",
    "fr": "Barpau"
  },
  {
    "id": 350,
    "en": "Milotic",
    "fr": "Milobellus"
  },
  {
    "id": 351,
    "en": "Castform",
    "fr": "Morphéo"
  },
  {
    "id": 352,
    "en": "Kecleon",
    "fr": "Kecleon"
  },
  {
    "id": 353,
    "en": "Shuppet",
    "fr": "Polichombr"
  },
  {
    "id": 354,
    "en": "Banette",
    "fr": "Branette"
  },
  {
    "id": 355,
    "en": "Duskull",
    "fr": "Skelénox"
  },
  {
    "id": 356,
    "en": "Dusclops",
    "fr": "Téraclope"
  },
  {
    "id": 357,
    "en": "Tropius",
    "fr": "Tropius"
  },
  {
    "id": 358,
    "en": "Chimecho",
    "fr": "Éoko"
  },
  {
    "id": 359,
    "en": "Absol",
    "fr": "Absol"
  },
  {
    "id": 360,
    "en": "Wynaut",
    "fr": "Okéoké"
  },
  {
    "id": 361,
    "en": "Snorunt",
    "fr": "Stalgamin"
  },
  {
    "id": 362,
    "en": "Glalie",
    "fr": "Oniglali"
  },
  {
    "id": 363,
    "en": "Spheal",
    "fr": "Obalie"
  },
  {
    "id": 364,
    "en": "Sealeo",
    "fr": "Phogleur"
  },
  {
    "id": 365,
    "en": "Walrein",
    "fr": "Kaimorse"
  },
  {
    "id": 366,
    "en": "Clamperl",
    "fr": "Coquiperl"
  },
  {
    "id": 367,
    "en": "Huntail",
    "fr": "Serpang"
  },
  {
    "id": 368,
    "en": "Gorebyss",
    "fr": "Rosabyss"
  },
  {
    "id": 369,
    "en": "Relicanth",
    "fr": "Relicanth"
  },
  {
    "id": 370,
    "en": "Luvdisc",
    "fr": "Lovdisc"
  },
  {
    "id": 371,
    "en": "Bagon",
    "fr": "Draby"
  },
  {
    "id": 372,
    "en": "Shelgon",
    "fr": "Drackhaus"
  },
  {
    "id": 373,
    "en": "Salamence",
    "fr": "Drattak"
  },
  {
    "id": 374,
    "en": "Beldum",
    "fr": "Terhal"
  },
  {
    "id": 375,
    "en": "Metang",
    "fr": "Métang"
  },
  {
    "id": 376,
    "en": "Metagross",
    "fr": "Métalosse"
  },
  {
    "id": 377,
    "en": "Regirock",
    "fr": "Regirock"
  },
  {
    "id": 378,
    "en": "Regice",
    "fr": "Regice"
  },
  {
    "id": 379,
    "en": "Registeel",
    "fr": "Registeel"
  },
  {
    "id": 380,
    "en": "Latias",
    "fr": "Latias"
  },
  {
    "id": 381,
    "en": "Latios",
    "fr": "Latios"
  },
  {
    "id": 382,
    "en": "Kyogre",
    "fr": "Kyogre"
  },
  {
    "id": 383,
    "en": "Groudon",
    "fr": "Groudon"
  },
  {
    "id": 384,
    "en": "Rayquaza",
    "fr": "Rayquaza"
  },
  {
    "id": 385,
    "en": "Jirachi",
    "fr": "Jirachi"
  },
  {
    "id": 386,
    "en": "Deoxys",
    "fr": "Deoxys"
  },
  {
    "id": 387,
    "en": "Turtwig",
    "fr": "Tortipouss"
  },
  {
    "id": 388,
    "en": "Grotle",
    "fr": "Boskara"
  },
  {
    "id": 389,
    "en": "Torterra",
    "fr": "Torterra"
  },
  {
    "id": 390,
    "en": "Chimchar",
    "fr": "Ouisticram"
  },
  {
    "id": 391,
    "en": "Monferno",
    "fr": "Chimpenfeu"
  },
  {
    "id": 392,
    "en": "Infernape",
    "fr": "Simiabraz"
  },
  {
    "id": 393,
    "en": "Piplup",
    "fr": "Tiplouf"
  },
  {
    "id": 394,
    "en": "Prinplup",
    "fr": "Prinplouf"
  },
  {
    "id": 395,
    "en": "Empoleon",
    "fr": "Pingoléon"
  },
  {
    "id": 396,
    "en": "Starly",
    "fr": "Étourmi"
  },
  {
    "id": 397,
    "en": "Staravia",
    "fr": "Étourvol"
  },
  {
    "id": 398,
    "en": "Staraptor",
    "fr": "Étouraptor"
  },
  {
    "id": 399,
    "en": "Bidoof",
    "fr": "Keunotor"
  },
  {
    "id": 400,
    "en": "Bibarel",
    "fr": "Castorno"
  },
  {
    "id": 401,
    "en": "Kricketot",
    "fr": "Crikzik"
  },
  {
    "id": 402,
    "en": "Kricketune",
    "fr": "Mélokrik"
  },
  {
    "id": 403,
    "en": "Shinx",
    "fr": "Lixy"
  },
  {
    "id": 404,
    "en": "Luxio",
    "fr": "Luxio"
  },
  {
    "id": 405,
    "en": "Luxray",
    "fr": "Luxray"
  },
  {
    "id": 406,
    "en": "Budew",
    "fr": "Rozbouton"
  },
  {
    "id": 407,
    "en": "Roserade",
    "fr": "Roserade"
  },
  {
    "id": 408,
    "en": "Cranidos",
    "fr": "Kranidos"
  },
  {
    "id": 409,
    "en": "Rampardos",
    "fr": "Charkos"
  },
  {
    "id": 410,
    "en": "Shieldon",
    "fr": "Dinoclier"
  },
  {
    "id": 411,
    "en": "Bastiodon",
    "fr": "Bastiodon"
  },
  {
    "id": 412,
    "en": "Burmy",
    "fr": "Cheniti"
  },
  {
    "id": 413,
    "en": "Wormadam",
    "fr": "Cheniselle"
  },
  {
    "id": 414,
    "en": "Mothim",
    "fr": "Papilord"
  },
  {
    "id": 415,
    "en": "Combee",
    "fr": "Apitrini"
  },
  {
    "id": 416,
    "en": "Vespiquen",
    "fr": "Apireine"
  },
  {
    "id": 417,
    "en": "Pachirisu",
    "fr": "Pachirisu"
  },
  {
    "id": 418,
    "en": "Buizel",
    "fr": "Mustébouée"
  },
  {
    "id": 419,
    "en": "Floatzel",
    "fr": "Mustéflott"
  },
  {
    "id": 420,
    "en": "Cherubi",
    "fr": "Ceribou"
  },
  {
    "id": 421,
    "en": "Cherrim",
    "fr": "Ceriflor"
  },
  {
    "id": 422,
    "en": "Shellos",
    "fr": "Sancoki"
  },
  {
    "id": 423,
    "en": "Gastrodon",
    "fr": "Tritosor"
  },
  {
    "id": 424,
    "en": "Ambipom",
    "fr": "Capidextre"
  },
  {
    "id": 425,
    "en": "Drifloon",
    "fr": "Baudrive"
  },
  {
    "id": 426,
    "en": "Drifblim",
    "fr": "Grodrive"
  },
  {
    "id": 427,
    "en": "Buneary",
    "fr": "Laporeille"
  },
  {
    "id": 428,
    "en": "Lopunny",
    "fr": "Lockpin"
  },
  {
    "id": 429,
    "en": "Mismagius",
    "fr": "Magirêve"
  },
  {
    "id": 430,
    "en": "Honchkrow",
    "fr": "Corboss"
  },
  {
    "id": 431,
    "en": "Glameow",
    "fr": "Chaglam"
  },
  {
    "id": 432,
    "en": "Purugly",
    "fr": "Chaffreux"
  },
  {
    "id": 433,
    "en": "Chingling",
    "fr": "Korillon"
  },
  {
    "id": 434,
    "en": "Stunky",
    "fr": "Moufouette"
  },
  {
    "id": 435,
    "en": "Skuntank",
    "fr": "Moufflair"
  },
  {
    "id": 436,
    "en": "Bronzor",
    "fr": "Archéomire"
  },
  {
    "id": 437,
    "en": "Bronzong",
    "fr": "Archéodong"
  },
  {
    "id": 438,
    "en": "Bonsly",
    "fr": "Manzaï"
  },
  {
    "id": 439,
    "en": "Mime Jr.",
    "fr": "Mime Jr."
  },
  {
    "id": 440,
    "en": "Happiny",
    "fr": "Ptiravi"
  },
  {
    "id": 441,
    "en": "Chatot",
    "fr": "Pijako"
  },
  {
    "id": 442,
    "en": "Spiritomb",
    "fr": "Spiritomb"
  },
  {
    "id": 443,
    "en": "Gible",
    "fr": "Griknot"
  },
  {
    "id": 444,
    "en": "Gabite",
    "fr": "Carmache"
  },
  {
    "id": 445,
    "en": "Garchomp",
    "fr": "Carchacrok"
  },
  {
    "id": 446,
    "en": "Munchlax",
    "fr": "Goinfrex"
  },
  {
    "id": 447,
    "en": "Riolu",
    "fr": "Riolu"
  },
  {
    "id": 448,
    "en": "Lucario",
    "fr": "Lucario"
  },
  {
    "id": 449,
    "en": "Hippopotas",
    "fr": "Hippopotas"
  },
  {
    "id": 450,
    "en": "Hippowdon",
    "fr": "Hippodocus"
  },
  {
    "id": 451,
    "en": "Skorupi",
    "fr": "Rapion"
  },
  {
    "id": 452,
    "en": "Drapion",
    "fr": "Drascore"
  },
  {
    "id": 453,
    "en": "Croagunk",
    "fr": "Cradopaud"
  },
  {
    "id": 454,
    "en": "Toxicroak",
    "fr": "Coatox"
  },
  {
    "id": 455,
    "en": "Carnivine",
    "fr": "Vortente"
  },
  {
    "id": 456,
    "en": "Finneon",
    "fr": "Écayon"
  },
  {
    "id": 457,
    "en": "Lumineon",
    "fr": "Luminéon"
  },
  {
    "id": 458,
    "en": "Mantyke",
    "fr": "Babimanta"
  },
  {
    "id": 459,
    "en": "Snover",
    "fr": "Blizzi"
  },
  {
    "id": 460,
    "en": "Abomasnow",
    "fr": "Blizzaroi"
  },
  {
    "id": 461,
    "en": "Weavile",
    "fr": "Dimoret"
  },
  {
    "id": 462,
    "en": "Magnezone",
    "fr": "Magnézone"
  },
  {
    "id": 463,
    "en": "Lickilicky",
    "fr": "Coudlangue"
  },
  {
    "id": 464,
    "en": "Rhyperior",
    "fr": "Rhinastoc"
  },
  {
    "id": 465,
    "en": "Tangrowth",
    "fr": "Bouldeneu"
  },
  {
    "id": 466,
    "en": "Electivire",
    "fr": "Élekable"
  },
  {
    "id": 467,
    "en": "Magmortar",
    "fr": "Maganon"
  },
  {
    "id": 468,
    "en": "Togekiss",
    "fr": "Togekiss"
  },
  {
    "id": 469,
    "en": "Yanmega",
    "fr": "Yanmega"
  },
  {
    "id": 470,
    "en": "Leafeon",
    "fr": "Phyllali"
  },
  {
    "id": 471,
    "en": "Glaceon",
    "fr": "Givrali"
  },
  {
    "id": 472,
    "en": "Gliscor",
    "fr": "Scorvol"
  },
  {
    "id": 473,
    "en": "Mamoswine",
    "fr": "Mammochon"
  },
  {
    "id": 474,
    "en": "Porygon-Z",
    "fr": "Porygon-Z"
  },
  {
    "id": 475,
    "en": "Gallade",
    "fr": "Gallame"
  },
  {
    "id": 476,
    "en": "Probopass",
    "fr": "Tarinorme"
  },
  {
    "id": 477,
    "en": "Dusknoir",
    "fr": "Noctunoir"
  },
  {
    "id": 478,
    "en": "Froslass",
    "fr": "Momartik"
  },
  {
    "id": 479,
    "en": "Rotom",
    "fr": "Motisma"
  },
  {
    "id": 480,
    "en": "Uxie",
    "fr": "Créhelf"
  },
  {
    "id": 481,
    "en": "Mesprit",
    "fr": "Créfollet"
  },
  {
    "id": 482,
    "en": "Azelf",
    "fr": "Créfadet"
  },
  {
    "id": 483,
    "en": "Dialga",
    "fr": "Dialga"
  },
  {
    "id": 484,
    "en": "Palkia",
    "fr": "Palkia"
  },
  {
    "id": 485,
    "en": "Heatran",
    "fr": "Heatran"
  },
  {
    "id": 486,
    "en": "Regigigas",
    "fr": "Regigigas"
  },
  {
    "id": 487,
    "en": "Giratina",
    "fr": "Giratina"
  },
  {
    "id": 488,
    "en": "Cresselia",
    "fr": "Cresselia"
  },
  {
    "id": 489,
    "en": "Phione",
    "fr": "Phione"
  },
  {
    "id": 490,
    "en": "Manaphy",
    "fr": "Manaphy"
  },
  {
    "id": 491,
    "en": "Darkrai",
    "fr": "Darkrai"
  },
  {
    "id": 492,
    "en": "Shaymin",
    "fr": "Shaymin"
  },
  {
    "id": 493,
    "en": "Arceus",
    "fr": "Arceus"
  },
  {
    "id": 494,
    "en": "Victini",
    "fr": "Victini"
  },
  {
    "id": 495,
    "en": "Snivy",
    "fr": "Vipélierre"
  },
  {
    "id": 496,
    "en": "Servine",
    "fr": "Lianaja"
  },
  {
    "id": 497,
    "en": "Serperior",
    "fr": "Majaspic"
  },
  {
    "id": 498,
    "en": "Tepig",
    "fr": "Gruikui"
  },
  {
    "id": 499,
    "en": "Pignite",
    "fr": "Grotichon"
  },
  {
    "id": 500,
    "en": "Emboar",
    "fr": "Roitiflam"
  },
  {
    "id": 501,
    "en": "Oshawott",
    "fr": "Moustillon"
  },
  {
    "id": 502,
    "en": "Dewott",
    "fr": "Mateloutre"
  },
  {
    "id": 503,
    "en": "Samurott",
    "fr": "Clamiral"
  },
  {
    "id": 504,
    "en": "Patrat",
    "fr": "Ratentif"
  },
  {
    "id": 505,
    "en": "Watchog",
    "fr": "Miradar"
  },
  {
    "id": 506,
    "en": "Lillipup",
    "fr": "Ponchiot"
  },
  {
    "id": 507,
    "en": "Herdier",
    "fr": "Ponchien"
  },
  {
    "id": 508,
    "en": "Stoutland",
    "fr": "Mastouffe"
  },
  {
    "id": 509,
    "en": "Purrloin",
    "fr": "Chacripan"
  },
  {
    "id": 510,
    "en": "Liepard",
    "fr": "Léopardus"
  },
  {
    "id": 511,
    "en": "Pansage",
    "fr": "Feuillajou"
  },
  {
    "id": 512,
    "en": "Simisage",
    "fr": "Feuiloutan"
  },
  {
    "id": 513,
    "en": "Pansear",
    "fr": "Flamajou"
  },
  {
    "id": 514,
    "en": "Simisear",
    "fr": "Flamoutan"
  },
  {
    "id": 515,
    "en": "Panpour",
    "fr": "Flotajou"
  },
  {
    "id": 516,
    "en": "Simipour",
    "fr": "Flotoutan"
  },
  {
    "id": 517,
    "en": "Munna",
    "fr": "Munna"
  },
  {
    "id": 518,
    "en": "Musharna",
    "fr": "Mushana"
  },
  {
    "id": 519,
    "en": "Pidove",
    "fr": "Poichigeon"
  },
  {
    "id": 520,
    "en": "Tranquill",
    "fr": "Colombeau"
  },
  {
    "id": 521,
    "en": "Unfezant",
    "fr": "Déflaisan"
  },
  {
    "id": 522,
    "en": "Blitzle",
    "fr": "Zébibron"
  },
  {
    "id": 523,
    "en": "Zebstrika",
    "fr": "Zéblitz"
  },
  {
    "id": 524,
    "en": "Roggenrola",
    "fr": "Nodulithe"
  },
  {
    "id": 525,
    "en": "Boldore",
    "fr": "Géolithe"
  },
  {
    "id": 526,
    "en": "Gigalith",
    "fr": "Gigalithe"
  },
  {
    "id": 527,
    "en": "Woobat",
    "fr": "Chovsourir"
  },
  {
    "id": 528,
    "en": "Swoobat",
    "fr": "Rhinolove"
  },
  {
    "id": 529,
    "en": "Drilbur",
    "fr": "Rototaupe"
  },
  {
    "id": 530,
    "en": "Excadrill",
    "fr": "Minotaupe"
  },
  {
    "id": 531,
    "en": "Audino",
    "fr": "Nanméouïe"
  },
  {
    "id": 532,
    "en": "Timburr",
    "fr": "Charpenti"
  },
  {
    "id": 533,
    "en": "Gurdurr",
    "fr": "Ouvrifier"
  },
  {
    "id": 534,
    "en": "Conkeldurr",
    "fr": "Bétochef"
  },
  {
    "id": 535,
    "en": "Tympole",
    "fr": "Tritonde"
  },
  {
    "id": 536,
    "en": "Palpitoad",
    "fr": "Batracné"
  },
  {
    "id": 537,
    "en": "Seismitoad",
    "fr": "Crapustule"
  },
  {
    "id": 538,
    "en": "Throh",
    "fr": "Judokrak"
  },
  {
    "id": 539,
    "en": "Sawk",
    "fr": "Karaclée"
  },
  {
    "id": 540,
    "en": "Sewaddle",
    "fr": "Larveyette"
  },
  {
    "id": 541,
    "en": "Swadloon",
    "fr": "Couverdure"
  },
  {
    "id": 542,
    "en": "Leavanny",
    "fr": "Manternel"
  },
  {
    "id": 543,
    "en": "Venipede",
    "fr": "Venipatte"
  },
  {
    "id": 544,
    "en": "Whirlipede",
    "fr": "Scobolide"
  },
  {
    "id": 545,
    "en": "Scolipede",
    "fr": "Brutapode"
  },
  {
    "id": 546,
    "en": "Cottonee",
    "fr": "Doudouvet"
  },
  {
    "id": 547,
    "en": "Whimsicott",
    "fr": "Farfaduvet"
  },
  {
    "id": 548,
    "en": "Petilil",
    "fr": "Chlorobule"
  },
  {
    "id": 549,
    "en": "Lilligant",
    "fr": "Fragilady"
  },
  {
    "id": 550,
    "en": "Basculin",
    "fr": "Bargantua"
  },
  {
    "id": 551,
    "en": "Sandile",
    "fr": "Mascaïman"
  },
  {
    "id": 552,
    "en": "Krokorok",
    "fr": "Escroco"
  },
  {
    "id": 553,
    "en": "Krookodile",
    "fr": "Crocorible"
  },
  {
    "id": 554,
    "en": "Darumaka",
    "fr": "Darumarond"
  },
  {
    "id": 555,
    "en": "Darmanitan",
    "fr": "Darumacho"
  },
  {
    "id": 556,
    "en": "Maractus",
    "fr": "Maracachi"
  },
  {
    "id": 557,
    "en": "Dwebble",
    "fr": "Crabicoque"
  },
  {
    "id": 558,
    "en": "Crustle",
    "fr": "Crabaraque"
  },
  {
    "id": 559,
    "en": "Scraggy",
    "fr": "Baggiguane"
  },
  {
    "id": 560,
    "en": "Scrafty",
    "fr": "Baggaïd"
  },
  {
    "id": 561,
    "en": "Sigilyph",
    "fr": "Cryptéro"
  },
  {
    "id": 562,
    "en": "Yamask",
    "fr": "Tutafeh"
  },
  {
    "id": 563,
    "en": "Cofagrigus",
    "fr": "Tutankafer"
  },
  {
    "id": 564,
    "en": "Tirtouga",
    "fr": "Carapagos"
  },
  {
    "id": 565,
    "en": "Carracosta",
    "fr": "Mégapagos"
  },
  {
    "id": 566,
    "en": "Archen",
    "fr": "Arkéapti"
  },
  {
    "id": 567,
    "en": "Archeops",
    "fr": "Aéroptéryx"
  },
  {
    "id": 568,
    "en": "Trubbish",
    "fr": "Miamiasme"
  },
  {
    "id": 569,
    "en": "Garbodor",
    "fr": "Miasmax"
  },
  {
    "id": 570,
    "en": "Zorua",
    "fr": "Zorua"
  },
  {
    "id": 571,
    "en": "Zoroark",
    "fr": "Zoroark"
  },
  {
    "id": 572,
    "en": "Minccino",
    "fr": "Chinchidou"
  },
  {
    "id": 573,
    "en": "Cinccino",
    "fr": "Pashmilla"
  },
  {
    "id": 574,
    "en": "Gothita",
    "fr": "Scrutella"
  },
  {
    "id": 575,
    "en": "Gothorita",
    "fr": "Mesmérella"
  },
  {
    "id": 576,
    "en": "Gothitelle",
    "fr": "Sidérella"
  },
  {
    "id": 577,
    "en": "Solosis",
    "fr": "Nucléos"
  },
  {
    "id": 578,
    "en": "Duosion",
    "fr": "Méios"
  },
  {
    "id": 579,
    "en": "Reuniclus",
    "fr": "Symbios"
  },
  {
    "id": 580,
    "en": "Ducklett",
    "fr": "Couaneton"
  },
  {
    "id": 581,
    "en": "Swanna",
    "fr": "Lakmécygne"
  },
  {
    "id": 582,
    "en": "Vanillite",
    "fr": "Sorbébé"
  },
  {
    "id": 583,
    "en": "Vanillish",
    "fr": "Sorboul"
  },
  {
    "id": 584,
    "en": "Vanilluxe",
    "fr": "Sorbouboul"
  },
  {
    "id": 585,
    "en": "Deerling",
    "fr": "Vivaldaim"
  },
  {
    "id": 586,
    "en": "Sawsbuck",
    "fr": "Haydaim"
  },
  {
    "id": 587,
    "en": "Emolga",
    "fr": "Emolga"
  },
  {
    "id": 588,
    "en": "Karrablast",
    "fr": "Carabing"
  },
  {
    "id": 589,
    "en": "Escavalier",
    "fr": "Lançargot"
  },
  {
    "id": 590,
    "en": "Foongus",
    "fr": "Trompignon"
  },
  {
    "id": 591,
    "en": "Amoonguss",
    "fr": "Gaulet"
  },
  {
    "id": 592,
    "en": "Frillish",
    "fr": "Viskuse"
  },
  {
    "id": 593,
    "en": "Jellicent",
    "fr": "Moyade"
  },
  {
    "id": 594,
    "en": "Alomomola",
    "fr": "Mamanbo"
  },
  {
    "id": 595,
    "en": "Joltik",
    "fr": "Statitik"
  },
  {
    "id": 596,
    "en": "Galvantula",
    "fr": "Mygavolt"
  },
  {
    "id": 597,
    "en": "Ferroseed",
    "fr": "Grindur"
  },
  {
    "id": 598,
    "en": "Ferrothorn",
    "fr": "Noacier"
  },
  {
    "id": 599,
    "en": "Klink",
    "fr": "Tic"
  },
  {
    "id": 600,
    "en": "Klang",
    "fr": "Clic"
  },
  {
    "id": 601,
    "en": "Klinklang",
    "fr": "Cliticlic"
  },
  {
    "id": 602,
    "en": "Tynamo",
    "fr": "Anchwatt"
  },
  {
    "id": 603,
    "en": "Eelektrik",
    "fr": "Lampéroie"
  },
  {
    "id": 604,
    "en": "Eelektross",
    "fr": "Ohmassacre"
  },
  {
    "id": 605,
    "en": "Elgyem",
    "fr": "Lewsor"
  },
  {
    "id": 606,
    "en": "Beheeyem",
    "fr": "Neitram"
  },
  {
    "id": 607,
    "en": "Litwick",
    "fr": "Funécire"
  },
  {
    "id": 608,
    "en": "Lampent",
    "fr": "Mélancolux"
  },
  {
    "id": 609,
    "en": "Chandelure",
    "fr": "Lugulabre"
  },
  {
    "id": 610,
    "en": "Axew",
    "fr": "Coupenotte"
  },
  {
    "id": 611,
    "en": "Fraxure",
    "fr": "Incisache"
  },
  {
    "id": 612,
    "en": "Haxorus",
    "fr": "Tranchodon"
  },
  {
    "id": 613,
    "en": "Cubchoo",
    "fr": "Polarhume"
  },
  {
    "id": 614,
    "en": "Beartic",
    "fr": "Polagriffe"
  },
  {
    "id": 615,
    "en": "Cryogonal",
    "fr": "Hexagel"
  },
  {
    "id": 616,
    "en": "Shelmet",
    "fr": "Escargaume"
  },
  {
    "id": 617,
    "en": "Accelgor",
    "fr": "Limaspeed"
  },
  {
    "id": 618,
    "en": "Stunfisk",
    "fr": "Limonde"
  },
  {
    "id": 619,
    "en": "Mienfoo",
    "fr": "Kungfouine"
  },
  {
    "id": 620,
    "en": "Mienshao",
    "fr": "Shaofouine"
  },
  {
    "id": 621,
    "en": "Druddigon",
    "fr": "Drakkarmin"
  },
  {
    "id": 622,
    "en": "Golett",
    "fr": "Gringolem"
  },
  {
    "id": 623,
    "en": "Golurk",
    "fr": "Golemastoc"
  },
  {
    "id": 624,
    "en": "Pawniard",
    "fr": "Scalpion"
  },
  {
    "id": 625,
    "en": "Bisharp",
    "fr": "Scalproie"
  },
  {
    "id": 626,
    "en": "Bouffalant",
    "fr": "Frison"
  },
  {
    "id": 627,
    "en": "Rufflet",
    "fr": "Furaiglon"
  },
  {
    "id": 628,
    "en": "Braviary",
    "fr": "Gueriaigle"
  },
  {
    "id": 629,
    "en": "Vullaby",
    "fr": "Vostourno"
  },
  {
    "id": 630,
    "en": "Mandibuzz",
    "fr": "Vaututrice"
  },
  {
    "id": 631,
    "en": "Heatmor",
    "fr": "Aflamanoir"
  },
  {
    "id": 632,
    "en": "Durant",
    "fr": "Fermite"
  },
  {
    "id": 633,
    "en": "Deino",
    "fr": "Solochi"
  },
  {
    "id": 634,
    "en": "Zweilous",
    "fr": "Diamat"
  },
  {
    "id": 635,
    "en": "Hydreigon",
    "fr": "Trioxhydre"
  },
  {
    "id": 636,
    "en": "Larvesta",
    "fr": "Pyronille"
  },
  {
    "id": 637,
    "en": "Volcarona",
    "fr": "Pyrax"
  },
  {
    "id": 638,
    "en": "Cobalion",
    "fr": "Cobaltium"
  },
  {
    "id": 639,
    "en": "Terrakion",
    "fr": "Terrakium"
  },
  {
    "id": 640,
    "en": "Virizion",
    "fr": "Viridium"
  },
  {
    "id": 641,
    "en": "Tornadus",
    "fr": "Boréas"
  },
  {
    "id": 642,
    "en": "Thundurus",
    "fr": "Fulguris"
  },
  {
    "id": 643,
    "en": "Reshiram",
    "fr": "Reshiram"
  },
  {
    "id": 644,
    "en": "Zekrom",
    "fr": "Zekrom"
  },
  {
    "id": 645,
    "en": "Landorus",
    "fr": "Démétéros"
  },
  {
    "id": 646,
    "en": "Kyurem",
    "fr": "Kyurem"
  },
  {
    "id": 647,
    "en": "Keldeo",
    "fr": "Keldeo"
  },
  {
    "id": 648,
    "en": "Meloetta",
    "fr": "Meloetta"
  },
  {
    "id": 649,
    "en": "Genesect",
    "fr": "Genesect"
  },
  {
    "id": 650,
    "en": "Chespin",
    "fr": "Marisson"
  },
  {
    "id": 651,
    "en": "Quilladin",
    "fr": "Boguérisse"
  },
  {
    "id": 652,
    "en": "Chesnaught",
    "fr": "Blindépique"
  },
  {
    "id": 653,
    "en": "Fennekin",
    "fr": "Feunnec"
  },
  {
    "id": 654,
    "en": "Braixen",
    "fr": "Roussil"
  },
  {
    "id": 655,
    "en": "Delphox",
    "fr": "Goupelin"
  },
  {
    "id": 656,
    "en": "Froakie",
    "fr": "Grenousse"
  },
  {
    "id": 657,
    "en": "Frogadier",
    "fr": "Croâporal"
  },
  {
    "id": 658,
    "en": "Greninja",
    "fr": "Amphinobi"
  },
  {
    "id": 659,
    "en": "Bunnelby",
    "fr": "Sapereau"
  },
  {
    "id": 660,
    "en": "Diggersby",
    "fr": "Excavarenne"
  },
  {
    "id": 661,
    "en": "Fletchling",
    "fr": "Passerouge"
  },
  {
    "id": 662,
    "en": "Fletchinder",
    "fr": "Braisillon"
  },
  {
    "id": 663,
    "en": "Talonflame",
    "fr": "Flambusard"
  },
  {
    "id": 664,
    "en": "Scatterbug",
    "fr": "Lépidonille"
  },
  {
    "id": 665,
    "en": "Spewpa",
    "fr": "Pérégrain"
  },
  {
    "id": 666,
    "en": "Vivillon",
    "fr": "Prismillon"
  },
  {
    "id": 667,
    "en": "Litleo",
    "fr": "Hélionceau"
  },
  {
    "id": 668,
    "en": "Pyroar",
    "fr": "Némélios"
  },
  {
    "id": 669,
    "en": "Flabébé",
    "fr": "Flabébé"
  },
  {
    "id": 670,
    "en": "Floette",
    "fr": "Floette"
  },
  {
    "id": 671,
    "en": "Florges",
    "fr": "Florges"
  },
  {
    "id": 672,
    "en": "Skiddo",
    "fr": "Cabriolaine"
  },
  {
    "id": 673,
    "en": "Gogoat",
    "fr": "Chevroum"
  },
  {
    "id": 674,
    "en": "Pancham",
    "fr": "Pandespiègle"
  },
  {
    "id": 675,
    "en": "Pangoro",
    "fr": "Pandarbare"
  },
  {
    "id": 676,
    "en": "Furfrou",
    "fr": "Couafarel"
  },
  {
    "id": 677,
    "en": "Espurr",
    "fr": "Psystigri"
  },
  {
    "id": 678,
    "en": "Meowstic",
    "fr": "Mistigrix"
  },
  {
    "id": 679,
    "en": "Honedge",
    "fr": "Monorpale"
  },
  {
    "id": 680,
    "en": "Doublade",
    "fr": "Dimoclès"
  },
  {
    "id": 681,
    "en": "Aegislash",
    "fr": "Exagide"
  },
  {
    "id": 682,
    "en": "Spritzee",
    "fr": "Fluvetin"
  },
  {
    "id": 683,
    "en": "Aromatisse",
    "fr": "Cocotine"
  },
  {
    "id": 684,
    "en": "Swirlix",
    "fr": "Sucroquin"
  },
  {
    "id": 685,
    "en": "Slurpuff",
    "fr": "Cupcanaille"
  },
  {
    "id": 686,
    "en": "Inkay",
    "fr": "Sepiatop"
  },
  {
    "id": 687,
    "en": "Malamar",
    "fr": "Sepiatroce"
  },
  {
    "id": 688,
    "en": "Binacle",
    "fr": "Opermine"
  },
  {
    "id": 689,
    "en": "Barbaracle",
    "fr": "Golgopathe"
  },
  {
    "id": 690,
    "en": "Skrelp",
    "fr": "Venalgue"
  },
  {
    "id": 691,
    "en": "Dragalge",
    "fr": "Kravarech"
  },
  {
    "id": 692,
    "en": "Clauncher",
    "fr": "Flingouste"
  },
  {
    "id": 693,
    "en": "Clawitzer",
    "fr": "Gamblast"
  },
  {
    "id": 694,
    "en": "Helioptile",
    "fr": "Galvaran"
  },
  {
    "id": 695,
    "en": "Heliolisk",
    "fr": "Iguolta"
  },
  {
    "id": 696,
    "en": "Tyrunt",
    "fr": "Ptyranidur"
  },
  {
    "id": 697,
    "en": "Tyrantrum",
    "fr": "Rexillius"
  },
  {
    "id": 698,
    "en": "Amaura",
    "fr": "Amagara"
  },
  {
    "id": 699,
    "en": "Aurorus",
    "fr": "Dragmara"
  },
  {
    "id": 700,
    "en": "Sylveon",
    "fr": "Nymphali"
  },
  {
    "id": 701,
    "en": "Hawlucha",
    "fr": "Brutalibré"
  },
  {
    "id": 702,
    "en": "Dedenne",
    "fr": "Dedenne"
  },
  {
    "id": 703,
    "en": "Carbink",
    "fr": "Strassie"
  },
  {
    "id": 704,
    "en": "Goomy",
    "fr": "Mucuscule"
  },
  {
    "id": 705,
    "en": "Sliggoo",
    "fr": "Colimucus"
  },
  {
    "id": 706,
    "en": "Goodra",
    "fr": "Muplodocus"
  },
  {
    "id": 707,
    "en": "Klefki",
    "fr": "Trousselin"
  },
  {
    "id": 708,
    "en": "Phantump",
    "fr": "Brocélôme"
  },
  {
    "id": 709,
    "en": "Trevenant",
    "fr": "Desséliande"
  },
  {
    "id": 710,
    "en": "Pumpkaboo",
    "fr": "Pitrouille"
  },
  {
    "id": 711,
    "en": "Gourgeist",
    "fr": "Banshitrouye"
  },
  {
    "id": 712,
    "en": "Bergmite",
    "fr": "Grelaçon"
  },
  {
    "id": 713,
    "en": "Avalugg",
    "fr": "Séracrawl"
  },
  {
    "id": 714,
    "en": "Noibat",
    "fr": "Sonistrelle"
  },
  {
    "id": 715,
    "en": "Noivern",
    "fr": "Bruyverne"
  },
  {
    "id": 716,
    "en": "Xerneas",
    "fr": "Xerneas"
  },
  {
    "id": 717,
    "en": "Yveltal",
    "fr": "Yveltal"
  },
  {
    "id": 718,
    "en": "Zygarde",
    "fr": "Zygarde"
  },
  {
    "id": 719,
    "en": "Diancie",
    "fr": "Diancie"
  },
  {
    "id": 720,
    "en": "Hoopa",
    "fr": "Hoopa"
  },
  {
    "id": 721,
    "en": "Volcanion",
    "fr": "Volcanion"
  },
  {
    "id": 722,
    "en": "Rowlet",
    "fr": "Brindibou"
  },
  {
    "id": 723,
    "en": "Dartrix",
    "fr": "Efflèche"
  },
  {
    "id": 724,
    "en": "Decidueye",
    "fr": "Archéduc"
  },
  {
    "id": 725,
    "en": "Litten",
    "fr": "Flamiaou"
  },
  {
    "id": 726,
    "en": "Torracat",
    "fr": "Matoufeu"
  },
  {
    "id": 727,
    "en": "Incineroar",
    "fr": "Félinferno"
  },
  {
    "id": 728,
    "en": "Popplio",
    "fr": "Otaquin"
  },
  {
    "id": 729,
    "en": "Brionne",
    "fr": "Otarlette"
  },
  {
    "id": 730,
    "en": "Primarina",
    "fr": "Oratoria"
  },
  {
    "id": 731,
    "en": "Pikipek",
    "fr": "Picassaut"
  },
  {
    "id": 732,
    "en": "Trumbeak",
    "fr": "Piclairon"
  },
  {
    "id": 733,
    "en": "Toucannon",
    "fr": "Bazoucan"
  },
  {
    "id": 734,
    "en": "Yungoos",
    "fr": "Manglouton"
  },
  {
    "id": 735,
    "en": "Gumshoos",
    "fr": "Argouste"
  },
  {
    "id": 736,
    "en": "Grubbin",
    "fr": "Larvibule"
  },
  {
    "id": 737,
    "en": "Charjabug",
    "fr": "Chrysapile"
  },
  {
    "id": 738,
    "en": "Vikavolt",
    "fr": "Lucanon"
  },
  {
    "id": 739,
    "en": "Crabrawler",
    "fr": "Crabagarre"
  },
  {
    "id": 740,
    "en": "Crabominable",
    "fr": "Crabominable"
  },
  {
    "id": 741,
    "en": "Oricorio",
    "fr": "Plumeline"
  },
  {
    "id": 742,
    "en": "Cutiefly",
    "fr": "Bombydou"
  },
  {
    "id": 743,
    "en": "Ribombee",
    "fr": "Rubombelle"
  },
  {
    "id": 744,
    "en": "Rockruff",
    "fr": "Rocabot"
  },
  {
    "id": 745,
    "en": "Lycanroc",
    "fr": "Lougaroc"
  },
  {
    "id": 746,
    "en": "Wishiwashi",
    "fr": "Froussardine"
  },
  {
    "id": 747,
    "en": "Mareanie",
    "fr": "Vorastérie"
  },
  {
    "id": 748,
    "en": "Toxapex",
    "fr": "Prédastérie"
  },
  {
    "id": 749,
    "en": "Mudbray",
    "fr": "Tiboudet"
  },
  {
    "id": 750,
    "en": "Mudsdale",
    "fr": "Bourrinos"
  },
  {
    "id": 751,
    "en": "Dewpider",
    "fr": "Araqua"
  },
  {
    "id": 752,
    "en": "Araquanid",
    "fr": "Tarenbulle"
  },
  {
    "id": 753,
    "en": "Fomantis",
    "fr": "Mimantis"
  },
  {
    "id": 754,
    "en": "Lurantis",
    "fr": "Floramantis"
  },
  {
    "id": 755,
    "en": "Morelull",
    "fr": "Spododo"
  },
  {
    "id": 756,
    "en": "Shiinotic",
    "fr": "Lampignon"
  },
  {
    "id": 757,
    "en": "Salandit",
    "fr": "Tritox"
  },
  {
    "id": 758,
    "en": "Salazzle",
    "fr": "Malamandre"
  },
  {
    "id": 759,
    "en": "Stufful",
    "fr": "Nounourson"
  },
  {
    "id": 760,
    "en": "Bewear",
    "fr": "Chelours"
  },
  {
    "id": 761,
    "en": "Bounsweet",
    "fr": "Croquine"
  },
  {
    "id": 762,
    "en": "Steenee",
    "fr": "Candine"
  },
  {
    "id": 763,
    "en": "Tsareena",
    "fr": "Sucreine"
  },
  {
    "id": 764,
    "en": "Comfey",
    "fr": "Guérilande"
  },
  {
    "id": 765,
    "en": "Oranguru",
    "fr": "Gouroutan"
  },
  {
    "id": 766,
    "en": "Passimian",
    "fr": "Quartermac"
  },
  {
    "id": 767,
    "en": "Wimpod",
    "fr": "Sovkipou"
  },
  {
    "id": 768,
    "en": "Golisopod",
    "fr": "Sarmuraï"
  },
  {
    "id": 769,
    "en": "Sandygast",
    "fr": "Bacabouh"
  },
  {
    "id": 770,
    "en": "Palossand",
    "fr": "Trépassable"
  },
  {
    "id": 771,
    "en": "Pyukumuku",
    "fr": "Concombaffe"
  },
  {
    "id": 772,
    "en": "Type: Null",
    "fr": "Type:0"
  },
  {
    "id": 773,
    "en": "Silvally",
    "fr": "Silvallié"
  },
  {
    "id": 774,
    "en": "Minior",
    "fr": "Météno"
  },
  {
    "id": 775,
    "en": "Komala",
    "fr": "Dodoala"
  },
  {
    "id": 776,
    "en": "Turtonator",
    "fr": "Boumata"
  },
  {
    "id": 777,
    "en": "Togedemaru",
    "fr": "Togedemaru"
  },
  {
    "id": 778,
    "en": "Mimikyu",
    "fr": "Mimiqui"
  },
  {
    "id": 779,
    "en": "Bruxish",
    "fr": "Denticrisse"
  },
  {
    "id": 780,
    "en": "Drampa",
    "fr": "Draïeul"
  },
  {
    "id": 781,
    "en": "Dhelmise",
    "fr": "Sinistrail"
  },
  {
    "id": 782,
    "en": "Jangmo-o",
    "fr": "Bébécaille"
  },
  {
    "id": 783,
    "en": "Hakamo-o",
    "fr": "Écaïd"
  },
  {
    "id": 784,
    "en": "Kommo-o",
    "fr": "Ékaïser"
  },
  {
    "id": 785,
    "en": "Tapu Koko",
    "fr": "Tokorico"
  },
  {
    "id": 786,
    "en": "Tapu Lele",
    "fr": "Tokopiyon"
  },
  {
    "id": 787,
    "en": "Tapu Bulu",
    "fr": "Tokotoro"
  },
  {
    "id": 788,
    "en": "Tapu Fini",
    "fr": "Tokopisco"
  },
  {
    "id": 789,
    "en": "Cosmog",
    "fr": "Cosmog"
  },
  {
    "id": 790,
    "en": "Cosmoem",
    "fr": "Cosmovum"
  },
  {
    "id": 791,
    "en": "Solgaleo",
    "fr": "Solgaleo"
  },
  {
    "id": 792,
    "en": "Lunala",
    "fr": "Lunala"
  },
  {
    "id": 793,
    "en": "Nihilego",
    "fr": "Zéroïd"
  },
  {
    "id": 794,
    "en": "Buzzwole",
    "fr": "Mouscoto"
  },
  {
    "id": 795,
    "en": "Pheromosa",
    "fr": "Cancrelove"
  },
  {
    "id": 796,
    "en": "Xurkitree",
    "fr": "Câblifère"
  },
  {
    "id": 797,
    "en": "Celesteela",
    "fr": "Bamboiselle"
  },
  {
    "id": 798,
    "en": "Kartana",
    "fr": "Katagami"
  },
  {
    "id": 799,
    "en": "Guzzlord",
    "fr": "Engloutyran"
  },
  {
    "id": 800,
    "en": "Necrozma",
    "fr": "Necrozma"
  },
  {
    "id": 801,
    "en": "Magearna",
    "fr": "Magearna"
  },
  {
    "id": 802,
    "en": "Marshadow",
    "fr": "Marshadow"
  },
  {
    "id": 803,
    "en": "Poipole",
    "fr": "Vémini"
  },
  {
    "id": 804,
    "en": "Naganadel",
    "fr": "Mandrillon"
  },
  {
    "id": 805,
    "en": "Stakataka",
    "fr": "Ama-Ama"
  },
  {
    "id": 806,
    "en": "Blacephalon",
    "fr": "Pierroteknik"
  },
  {
    "id": 807,
    "en": "Zeraora",
    "fr": "Zeraora"
  },
  {
    "id": 808,
    "en": "Meltan",
    "fr": "Meltan"
  },
  {
    "id": 809,
    "en": "Melmetal",
    "fr": "Melmetal"
  },
  {
    "id": 810,
    "en": "Grookey",
    "fr": "Ouistempo"
  },
  {
    "id": 811,
    "en": "Thwackey",
    "fr": "Badabouin"
  },
  {
    "id": 812,
    "en": "Rillaboom",
    "fr": "Gorythmic"
  },
  {
    "id": 813,
    "en": "Scorbunny",
    "fr": "Flambino"
  },
  {
    "id": 814,
    "en": "Raboot",
    "fr": "Lapyro"
  },
  {
    "id": 815,
    "en": "Cinderace",
    "fr": "Pyrobut"
  },
  {
    "id": 816,
    "en": "Sobble",
    "fr": "Larméléon"
  },
  {
    "id": 817,
    "en": "Drizzile",
    "fr": "Arrozard"
  },
  {
    "id": 818,
    "en": "Inteleon",
    "fr": "Lézargus"
  },
  {
    "id": 819,
    "en": "Skwovet",
    "fr": "Rongourmand"
  },
  {
    "id": 820,
    "en": "Greedent",
    "fr": "Rongrigou"
  },
  {
    "id": 821,
    "en": "Rookidee",
    "fr": "Minisange"
  },
  {
    "id": 822,
    "en": "Corvisquire",
    "fr": "Bleuseille"
  },
  {
    "id": 823,
    "en": "Corviknight",
    "fr": "Corvaillus"
  },
  {
    "id": 824,
    "en": "Blipbug",
    "fr": "Larvadar"
  },
  {
    "id": 825,
    "en": "Dottler",
    "fr": "Coléodôme"
  },
  {
    "id": 826,
    "en": "Orbeetle",
    "fr": "Astronelle"
  },
  {
    "id": 827,
    "en": "Nickit",
    "fr": "Goupilou"
  },
  {
    "id": 828,
    "en": "Thievul",
    "fr": "Roublenard"
  },
  {
    "id": 829,
    "en": "Gossifleur",
    "fr": "Tournicoton"
  },
  {
    "id": 830,
    "en": "Eldegoss",
    "fr": "Blancoton"
  },
  {
    "id": 831,
    "en": "Wooloo",
    "fr": "Moumouton"
  },
  {
    "id": 832,
    "en": "Dubwool",
    "fr": "Moumouflon"
  },
  {
    "id": 833,
    "en": "Chewtle",
    "fr": "Khélocrok"
  },
  {
    "id": 834,
    "en": "Drednaw",
    "fr": "Torgamord"
  },
  {
    "id": 835,
    "en": "Yamper",
    "fr": "Voltoutou"
  },
  {
    "id": 836,
    "en": "Boltund",
    "fr": "Fulgudog"
  },
  {
    "id": 837,
    "en": "Rolycoly",
    "fr": "Charbi"
  },
  {
    "id": 838,
    "en": "Carkol",
    "fr": "Wagomine"
  },
  {
    "id": 839,
    "en": "Coalossal",
    "fr": "Monthracite"
  },
  {
    "id": 840,
    "en": "Applin",
    "fr": "Verpom"
  },
  {
    "id": 841,
    "en": "Flapple",
    "fr": "Pomdrapi"
  },
  {
    "id": 842,
    "en": "Appletun",
    "fr": "Dratatin"
  },
  {
    "id": 843,
    "en": "Silicobra",
    "fr": "Dunaja"
  },
  {
    "id": 844,
    "en": "Sandaconda",
    "fr": "Dunaconda"
  },
  {
    "id": 845,
    "en": "Cramorant",
    "fr": "Nigosier"
  },
  {
    "id": 846,
    "en": "Arrokuda",
    "fr": "Embrochet"
  },
  {
    "id": 847,
    "en": "Barraskewda",
    "fr": "Hastacuda"
  },
  {
    "id": 848,
    "en": "Toxel",
    "fr": "Toxizap"
  },
  {
    "id": 849,
    "en": "Toxtricity",
    "fr": "Salarsen"
  },
  {
    "id": 850,
    "en": "Sizzlipede",
    "fr": "Grillepattes"
  },
  {
    "id": 851,
    "en": "Centiskorch",
    "fr": "Scolocendre"
  },
  {
    "id": 852,
    "en": "Clobbopus",
    "fr": "Poulpaf"
  },
  {
    "id": 853,
    "en": "Grapploct",
    "fr": "Krakos"
  },
  {
    "id": 854,
    "en": "Sinistea",
    "fr": "Théffroi"
  },
  {
    "id": 855,
    "en": "Polteageist",
    "fr": "Polthégeist"
  },
  {
    "id": 856,
    "en": "Hatenna",
    "fr": "Bibichut"
  },
  {
    "id": 857,
    "en": "Hattrem",
    "fr": "Chapotus"
  },
  {
    "id": 858,
    "en": "Hatterene",
    "fr": "Sorcilence"
  },
  {
    "id": 859,
    "en": "Impidimp",
    "fr": "Grimalin"
  },
  {
    "id": 860,
    "en": "Morgrem",
    "fr": "Fourbelin"
  },
  {
    "id": 861,
    "en": "Grimmsnarl",
    "fr": "Angoliath"
  },
  {
    "id": 862,
    "en": "Obstagoon",
    "fr": "Ixon"
  },
  {
    "id": 863,
    "en": "Perrserker",
    "fr": "Berserkatt"
  },
  {
    "id": 864,
    "en": "Cursola",
    "fr": "Corayôme"
  },
  {
    "id": 865,
    "en": "Sirfetch’d",
    "fr": "Palarticho"
  },
  {
    "id": 866,
    "en": "Mr. Rime",
    "fr": "M. Glaquette"
  },
  {
    "id": 867,
    "en": "Runerigus",
    "fr": "Tutétékri"
  },
  {
    "id": 868,
    "en": "Milcery",
    "fr": "Crèmy"
  },
  {
    "id": 869,
    "en": "Alcremie",
    "fr": "Charmilly"
  },
  {
    "id": 870,
    "en": "Falinks",
    "fr": "Hexadron"
  },
  {
    "id": 871,
    "en": "Pincurchin",
    "fr": "Wattapik"
  },
  {
    "id": 872,
    "en": "Snom",
    "fr": "Frissonille"
  },
  {
    "id": 873,
    "en": "Frosmoth",
    "fr": "Beldeneige"
  },
  {
    "id": 874,
    "en": "Stonjourner",
    "fr": "Dolman"
  },
  {
    "id": 875,
    "en": "Eiscue",
    "fr": "Bekaglaçon"
  },
  {
    "id": 876,
    "en": "Indeedee",
    "fr": "Wimessir"
  },
  {
    "id": 877,
    "en": "Morpeko",
    "fr": "Morpeko"
  },
  {
    "id": 878,
    "en": "Cufant",
    "fr": "Charibari"
  },
  {
    "id": 879,
    "en": "Copperajah",
    "fr": "Pachyradjah"
  },
  {
    "id": 880,
    "en": "Dracozolt",
    "fr": "Galvagon"
  },
  {
    "id": 881,
    "en": "Arctozolt",
    "fr": "Galvagla"
  },
  {
    "id": 882,
    "en": "Dracovish",
    "fr": "Hydragon"
  },
  {
    "id": 883,
    "en": "Arctovish",
    "fr": "Hydragla"
  },
  {
    "id": 884,
    "en": "Duraludon",
    "fr": "Duralugon"
  },
  {
    "id": 885,
    "en": "Dreepy",
    "fr": "Fantyrm"
  },
  {
    "id": 886,
    "en": "Drakloak",
    "fr": "Dispareptil"
  },
  {
    "id": 887,
    "en": "Dragapult",
    "fr": "Lanssorien"
  },
  {
    "id": 888,
    "en": "Zacian",
    "fr": "Zacian"
  },
  {
    "id": 889,
    "en": "Zamazenta",
    "fr": "Zamazenta"
  },
  {
    "id": 890,
    "en": "Eternatus",
    "fr": "Éthernatos"
  },
  {
    "id": 891,
    "en": "Kubfu",
    "fr": "Wushours"
  },
  {
    "id": 892,
    "en": "Urshifu",
    "fr": "Shifours"
  },
  {
    "id": 893,
    "en": "Zarude",
    "fr": "Zarude"
  },
  {
    "id": 894,
    "en": "Regieleki",
    "fr": "Regieleki"
  },
  {
    "id": 895,
    "en": "Regidrago",
    "fr": "Regidrago"
  },
  {
    "id": 896,
    "en": "Glastrier",
    "fr": "Blizzeval"
  },
  {
    "id": 897,
    "en": "Spectrier",
    "fr": "Spectreval"
  },
  {
    "id": 898,
    "en": "Calyrex",
    "fr": "Sylveroy"
  }
];
